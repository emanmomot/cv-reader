
 
const { Client } = require("node-osc");
const client = new Client('127.0.0.1', 1234);

var aJackVals = [0,0,0,0,0,0];

const SerialPort = require('serialport');

const Readline = require('@serialport/parser-readline');
const port = new SerialPort('/dev/tty.usbmodem66263201', { baudRate: 9600 }, function (err) {
  if (err) {
    return console.log('Error: ', err.message)
  } else {
  console.log('serial port open');
    const parser = port.pipe(new Readline({ delimiter: '\n' }));
    parser.on('data', data => {
      var elems = data.split(":");
      if(elems[0] && elems[1]) {
        const val = parseInt(elems[1]);
        if (isNaN(val)) { 
          console.log("Invalid value: " + elems[1] + " at path: " + elems[0]);
          return;
        }


        if(elems[0][1] != '0') {
          return;
        }
        var oscPath = "";
        if(elems[0][0] == 'A') {
          oscPath = '/cv_reader/a_jack/' + elems[0][1];
        } else if( elems[0][0] == 'D') {
          oscPath = '/cv_reader/d_jack/' + elems[0][1];
        } else {
          console.log("unrecognized path: " + elems[0]);
        }

        aJackVals[0] = val;
      }
    });
  }
})

function sendAJackVals() {
  var oscPath = '/cv_reader/a_jack/0';
  var val = aJackVals[0];
  // console.log("sending val: " + val + " on: " + oscPath);
  client.send(oscPath, val);  
}

setInterval(sendAJackVals, 1000/60);

