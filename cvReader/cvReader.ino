
const int numAJacks = 6;
const int numDJacks = 4;
const int epsilon = 5;
const int numFilteredReads = 4;

const int potPins[] = {A14, A15, A16, A17, A18, A19};
const int aJackPins[] = {A0, A1, A2, A3, A4, A5};
const int dJackPins[] = {26, 25, 24, 12};
const int ledRPins[] = {21, 23, 8, 6, 4, 2};
const int ledGPins[] = {20, 22, 9, 7, 5, 3};

int aJackVals[numAJacks];

float aValToUnitBall(int val) {
  return (val - 512) / 512.0f;
}

int filteredRead(int pinNum) {
  analogRead(pinNum);
  int sum = 0;
  for(int i = 0; i < numFilteredReads; i++) {
    sum += analogRead(pinNum);
  }
  analogRead(pinNum);
  return sum / numFilteredReads;
}

float updateAJack(int jackNum) {
  int potVal = analogRead(potPins[jackNum]);
  int jackVal = analogRead(aJackPins[jackNum]);    

  float unitPotVal = aValToUnitBall(potVal);
  int scaledJackVal = (int)(unitPotVal * jackVal);
  
  if(scaledJackVal > epsilon) {
    analogWrite(ledGPins[jackNum], scaledJackVal);
    analogWrite(ledRPins[jackNum], 0);
  } else if(scaledJackVal < -epsilon) {
    analogWrite(ledGPins[jackNum], 0);
    analogWrite(ledRPins[jackNum], -scaledJackVal);
  } else {
    analogWrite(ledGPins[jackNum], 0);
    analogWrite(ledRPins[jackNum], 0);
  }

  return scaledJackVal;
}

void sendAJackVal(int jackNum, int val) {
    String msg = "";
    msg.concat("A");
    msg.concat(jackNum);
    msg.concat(":");
    msg.concat(val);
    Serial.println(msg);
}

void setup() {
  Serial.begin(9600);

  for(int i = 0; i < numDJacks; i++) {
    pinMode(dJackPins[i], OUTPUT);
  }
}

void loop() {
  for(int i = 0; i < numAJacks; i++) {
    aJackVals[i] = updateAJack(i);
    sendAJackVal(i, aJackVals[i]);
  }
}
